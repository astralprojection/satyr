const std = @import("std");
const flag = @import("flag.zig");

fn passthrough(x: []const u8) []const u8 {
    return x;
}

test "flag.boolean" {
    var dest: bool = false;
    const f = flag.boolean(&dest, "a", false, "works");
    try std.testing.expectEqualStrings(f.name, "a");
    try std.testing.expectEqualStrings(f.usage, "works");
    try std.testing.expectEqual(f.boolVal, true);
    f.init("true");
    try std.testing.expectEqual(dest, true);
    f.init("FALSE");
    try std.testing.expectEqual(dest, false);
}


test "flag.func" {
    var dest: []const u8 = "";
    const f = flag.func(&dest, "a", "works", passthrough);
    try std.testing.expectEqualStrings(f.name, "a");
    try std.testing.expectEqualStrings(f.usage, "works");
    try std.testing.expectEqual(f.boolVal, false);
    f.init("works");
    try std.testing.expectEqualStrings(dest, "works");
}

test "flag.int64" {
    var dest: i64 = 0;
    const f = flag.int64(&dest, "a", 1, "works");
    try std.testing.expectEqualStrings(f.name, "a");
    try std.testing.expectEqualStrings(f.usage, "works");
    try std.testing.expectEqual(f.boolVal, false);
    f.init("0");
    try std.testing.expectEqual(dest, 1);
    f.init("2");
    try std.testing.expectEqual(dest, 2);
}
