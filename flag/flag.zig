const fmt = @import("std").fmt;
const log = @import("std").log;
const mem = @import("std").mem;

pub const Flag = struct {
    // name is the --name parsed from argv
    name: []const u8,
    // usage is the description of how to use this flag
    usage: []const u8,
    // init sets
    init: *const fn ([]const u8) void,
    boolVal: bool = false,
};


pub fn boolean(out: *bool, name: []const u8, defaultVal: bool, usage: []const u8) Flag {
    return .{
        .name = name,
        .usage = usage,
        .init = setterBool(out, defaultVal),
        .boolVal = true,
    };
}

pub fn func(out: *[]const u8, name: []const u8, usage: []const u8, fun: *const fn ([]const u8) []const u8) Flag {
    return .{
        .name = name,
        .usage = usage,
        .init = setterString(out, fun),
    };
}

pub fn int64(out: *i64, name: []const u8, defaultVal: i64, usage: []const u8) Flag {
    return .{
        .name = name,
        .usage = usage,
        .init = setterI64(out, defaultVal),
    };
}

inline fn setterString(
    out: *[]const u8,
    fun: *const fn ([]const u8) []const u8,
) *const fn ([]const u8) void {
    return (opaque {
        var outPtr: *[]const u8 = undefined;
        var work: *const fn ([]const u8) []const u8 = undefined;

        pub fn init(o: *[]const u8, f: *const fn ([]const u8) []const u8) *const @TypeOf(run) {
            outPtr = o;
            work = f;
            return &run;
        }

        fn run(val: []const u8) void {
            outPtr.* = work(val);
        }
    }).init(out, fun);
}

inline fn setterI64(
    out: *i64,
    default: i64,
) *const fn ([]const u8) void {
    return (opaque {
        var outPtr: *i64 = undefined;
        var defaultVal: i64 = 0;

        pub fn init(o: *i64, d: i64) *const @TypeOf(run) {
            outPtr = o;
            defaultVal = d;
            return &run;
        }

        fn run(val: []const u8) void {
            if (val.len == 0) {
                outPtr.* = defaultVal;
            } else {
                outPtr.* = fmt.parseInt(i64, val, 10) catch unreachable;
            }
            if (outPtr.* == 0) {
                outPtr.* = defaultVal;
            }
        }
    }).init(out, default);
}

inline fn setterBool(
    out: *bool,
    default: bool,
) *const fn ([]const u8) void {
    return (opaque {
        var outPtr: *bool = undefined;
        var defaultVal: bool = false;

        pub fn init(o: *bool, d: bool) *const @TypeOf(run) {
            outPtr = o;
            defaultVal = d;
            return &run;
        }

        fn run(val: []const u8) void {
            if (val.len == 0) {
                outPtr.* = defaultVal;
            } else {
                if (mem.eql(u8, val, "true") or mem.eql(u8, val, "True") or mem.eql(u8, val, "TRUE")) {
                    outPtr.* = true;
                    return;
                }
                if (mem.eql(u8, val, "false") or mem.eql(u8, val, "False") or mem.eql(u8, val, "FALSE")) {
                    outPtr.* = false;
                    return;
                }
                outPtr.* = defaultVal;
            }
        }
    }).init(out, default);
}
