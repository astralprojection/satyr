const std = @import("std");
const mem = std.mem;

pub const Set = struct {
    allocator: mem.Allocator,
    argv: std.ArrayList([]const u8),
    setterNoArg: std.StringHashMap(*const fn () void),
    setterOneArg: std.StringHashMap(*const fn ([]const u8) void),
    setterUsage: std.StringHashMap([]const u8),

    const This = @This();

    pub fn init(alloc: mem.Allocator) Set {
        return .{
            .allocator = alloc,
            .argv = std.ArrayList([]const u8).init(alloc),
            .setterNoArg = std.StringHashMap(*const fn () void).init(alloc),
            .setterOneArg = std.StringHashMap(*const fn ([]const u8) void).init(alloc),
            .setterUsage = std.StringHashMap([]const u8).init(alloc),
        };
    }

    pub fn deinit(this: *This) void {
        this.argv.deinit();
        this.setterNoArg.deinit();
        this.setterOneArg.deinit();
        //this.setterUsage().deinit();
    }
};
