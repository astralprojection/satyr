#!/usr/bin/env sh

# fmt invokes `zig fmt` on all zig files in the repo

find . -type f -name "*.zig" -exec zig fmt {} \;