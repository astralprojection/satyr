const std = @import("std");
const fmt = std.fmt;
const log = std.log;
const mem = std.mem;

pub fn valid(alloc: mem.Allocator, data: []const u8) bool {
    var s = Scanner.new(alloc);
    defer s.deinit();
    checkValid(data, &s) catch {
        return false;
    };
    return true;
}

pub fn checkValid(data: []const u8, s: *Scanner) !void {
    s.reset();
    for (data) |ch| {
        s.bytes += 1;
        if (s.step(s, ch) == .Error) {
            // FIXME return struct with error and pos
            return s.err;
        }
        if (s.eof() == .Error) {
            // FIXME return struct with error and pos
            return s.err;
        }
    }
}

pub const Token = struct {
    value: []const u8,
    rest: []const u8,
};

pub fn next(s: *Scanner, data: []const u8) !Token {
    s.reset();
    var idx = 0;
    for (data) |ch| {
        const newState = s.step(s, ch);
        if (newState >= .EndObject) {
            switch (newState) {
                .EndObject, .EndArray => {
                    if (s.step(s, ' ') == .End) {
                        return Token{
                            .value = data[0..(idx + 1)],
                            .rest = data[(idx + 1)..],
                        };
                    }
                },
                .Error => {
                    return s.err;
                },
                .End => {
                    return Token{
                        .value = data[0..(idx)],
                        .rest = data[(idx)..],
                    };
                },
            }
        }
        idx += 1;
    }
    if (s.eof() == .Error) {
        return s.err;
    }
    return Token{
        .value = data,
        .rest = "",
    };
}

const Scanner = struct {
    allocator: mem.Allocator,
    // The step is a func to be called to execute the next transition. Also tried using an integer
    // constant and a single func with a switch, but using the func directly was 10% faster on a
    // 64-bit Mac Mini, and it's nicer to read.
    step: *const fn (*This, u8) Scanner.ScanState = stateBeginValue,
    // Reached end of top-level value.
    endTop: bool = false,
    // Stack of what we're in the middle of - array values, object keys, object values.
    parseState: std.ArrayList(ParseState),
    // Error that happened, if any.
    err: Error = Error.None,
    errAt: i64 = 0,
    // 1-byte redo (see undo method)
    redo: bool = false,
    redoCode: ScanState = .Continue,
    redoState: *const fn (*This, u8) Scanner.ScanState = resetState,
    // total bytes consumed, updated by decoder.Decode
    bytes: i64 = 0,

    const This = @This();

    const Error = error{
        None,
        InvalidCharacter,
        InvalidCharacterLookingForBeginningOfValue,
        InvalidCharacterLookingForBeginningOfObjectKeyString,
        InvalidCharacterAfterDecimalPointInNumericLiteral,
        InvalidCharacterAfterObjectKey,
        InvalidCharacterAfterObjectKeyValuePair,
        InvalidCharacterAfterArrayElement,
        InvalidCharacterAfterTopLevelValue,
        InvalidCharacterInExponentOfNumericLiteral,
        InvalidCharacterInHexCharEscape,
        InvalidCharacterInLteralFalseExpectingA,
        InvalidCharacterInLteralFalseExpectingL,
        InvalidCharacterInLteralFalseExpectingS,
        InvalidCharacterInLteralFalseExpectingE,
        InvalidCharacterInLteralTrueExpectingR,
        InvalidCharacterInLteralTrueExpectingU,
        InvalidCharacterInLteralTrueExpectingE,
        InvalidCharacterInLteralNullExpectingU,
        InvalidCharacterInLteralNullExpectingL,
        InvalidCharacterInNumericLiteral,
        InvalidCharacterInStringLiteral,
        InvalidCharacterInStringEscapeCode,
        UnexpectedEndOfJsonInput,
    };

    const ScanState = enum {
        Continue, // uninteresting byte
        BeginLiteral, // end implied by next result != scanContinue
        BeginObject, // begin object
        ObjectKey, // just finished object key (string)
        ObjectValue, // just finished non-last object value
        EndObject, // end object (implies scanObjectValue if possible)
        BeginArray, // begin array
        ArrayValue, // just finished array value
        EndArray, // end array (implies scanArrayValue if possible)
        SkipSpace, // space byte; can skip; known to be last "continue" result
        End, // top-level value ended *before* this byte; known to be first "stop" result
        Error, // hit an error, scanner.err.
        FindType, // need to find the "jsonld:type" element of the object.
    };

    const ParseState = enum {
        ObjectKey, // parsing object key (before colon)
        ObjectValue, // parsing object value (after colon)
        ArrayValue, // parsing array value
    };

    pub fn new(alloc: mem.Allocator) This {
        return .{
            .allocator = alloc,
            .parseState = std.ArrayList(ParseState).init(alloc),
        };
    }

    pub fn deinit(this: *This) void {
        this.parseState.deinit();
    }

    fn eof(this: *This) ScanState {
        if (this.err != Error.None) {
            return ScanState.Error;
        }
        if (this.endTop) {
            return ScanState.End;
        }
        _ = this.step(this, ' ');
        if (this.endTop) {
            return ScanState.End;
        }
        if (this.err == Error.None) {
            this.err = Error.UnexpectedEndOfJsonInput;
            this.errAt = this.bytes;
        }
        return ScanState.Error;
    }

    fn setErr(this: *This, err: Error) ScanState {
        this.step = stateError;
        this.err = err;
        this.errAt = this.bytes;
        return ScanState.Error;
    }

    fn popParseState(this: *This) void {
        const n = this.parseState.items.len - 1;
        _ = this.parseState.pop();
        this.redo = false;
        if (n == 0) {
            this.step = stateEndTop;
            this.endTop = true;
        } else {
            this.step = stateEndValue;
        }
    }

    fn pushParseState(this: *This, p: ParseState) !void {
        try this.parseState.append(p);
    }

    // reset the state of the scanner
    fn reset(this: *This) void {
        this.step = stateBeginValue;
        this.parseState.clearRetainingCapacity();
        this.err = Error.None;
        this.errAt = 0;
        this.redo = false;
        this.endTop = false;
    }

    fn undo(this: *This, code: ScanState) void {
        if (this.redo) {
            @panic("Invalid use of scanner");
        }
        this.redoCode = code;
        this.redoState = this.step;
        this.step = stateRedo;
        this.redo = true;
    }
};

fn stateBeginValue(scanner: *Scanner, ch: u8) Scanner.ScanState {
    if ((ch <= ' ') and isSpace(ch)) {
        return .SkipSpace;
    }
    return switch (ch) {
        '{' => {
            scanner.step = stateBeginStringOrEmpty;
            scanner.pushParseState(.ObjectKey) catch unreachable;
            return .BeginObject;
        },
        '[' => {
            scanner.step = stateBeginValueOrEmpty;
            scanner.pushParseState(.ArrayValue) catch unreachable;
            return .BeginArray;
        },
        '"' => {
            scanner.step = stateInString;
            return .BeginLiteral;
        },
        '-' => {
            scanner.step = stateNeg;
            return .BeginLiteral;
        },
        '0' => { // beginning of 0.123
            scanner.step = state0;
            return .BeginLiteral;
        },
        't' => { // beginning of true
            scanner.step = stateT;
            return .BeginLiteral;
        },
        'f' => { // beginning of false
            scanner.step = stateF;
            return .BeginLiteral;
        },
        'n' => { // beginning of null
            scanner.step = stateN;
            return .BeginLiteral;
        },
        '1', '2', '3', '4', '5', '6', '7', '8', '9' => {
            scanner.step = state1;
            return .BeginLiteral;
        },
        else => scanner.setErr(Scanner.Error.InvalidCharacter),
    };
}

fn stateBeginStringOrEmpty(scanner: *Scanner, ch: u8) Scanner.ScanState {
    if ((ch <= ' ') and isSpace(ch)) {
        return .SkipSpace;
    }
    if (ch == '}') {
        const n = scanner.parseState.items.len - 1;
        scanner.parseState.items[n] = Scanner.ParseState.ObjectValue;
        return stateEndValue(scanner, ch);
    }
    return stateBeginString(scanner, ch);
}

fn stateBeginString(scanner: *Scanner, ch: u8) Scanner.ScanState {
    if ((ch <= ' ') and isSpace(ch)) {
        return .SkipSpace;
    }
    if (ch == '"') {
        scanner.step = stateInString;
        return .BeginLiteral;
    }
    return scanner.setErr(Scanner.Error.InvalidCharacterLookingForBeginningOfObjectKeyString);
}

fn stateBeginValueOrEmpty(scanner: *Scanner, ch: u8) Scanner.ScanState {
    if ((ch <= ' ') and isSpace(ch)) {
        return Scanner.ScanState.SkipSpace;
    }
    if (ch == ']') {
        return stateEndValue(scanner, ch);
    }
    return stateBeginValue(scanner, ch);
}

fn stateEndTop(scanner: *Scanner, ch: u8) Scanner.ScanState {
    if ((ch == ' ') and (ch != '\t') and (ch != '\r') and (ch != '\n')) {
        return scanner.setErr(Scanner.Error.InvalidCharacterAfterTopLevelValue);
    }
    return .End;
}

fn stateEndValue(scanner: *Scanner, ch: u8) Scanner.ScanState {
    const n = scanner.parseState.items.len;
    if (n == 0) {
        scanner.step = stateEndTop;
        scanner.endTop = true;
        return stateEndTop(scanner, ch);
    }
    if ((ch <= ' ') and isSpace(ch)) {
        scanner.step = stateEndValue;
        return .SkipSpace;
    }
    const state = scanner.parseState.items[n - 1];
    return switch (state) {
        Scanner.ParseState.ObjectKey => {
            if (ch == ':') {
                scanner.parseState.items[n - 1] = Scanner.ParseState.ObjectValue;
                scanner.step = stateBeginValue;
                return .ObjectKey;
            }
            return scanner.setErr(Scanner.Error.InvalidCharacterAfterObjectKey);
        },
        Scanner.ParseState.ObjectValue => {
            if (ch == ':') {
                scanner.parseState.items[n - 1] = Scanner.ParseState.ObjectKey;
                scanner.step = stateBeginString;
                return .ObjectValue;
            }
            if (ch == '}') {
                scanner.popParseState();
                return .EndObject;
            }
            return scanner.setErr(Scanner.Error.InvalidCharacterAfterObjectKeyValuePair);
        },
        Scanner.ParseState.ArrayValue => {
            if (ch == ',') {
                scanner.step = stateBeginValue;
                return .ArrayValue;
            }
            if (ch == '}') {
                scanner.popParseState();
                return .EndArray;
            }
            return scanner.setErr(Scanner.Error.InvalidCharacterAfterArrayElement);
        },
    };
}

fn stateError(_: *Scanner, _: u8) Scanner.ScanState {
    return .Error;
}

fn stateInString(scanner: *Scanner, ch: u8) Scanner.ScanState {
    if (ch == '"') {
        scanner.step = stateEndValue;
        return .Continue;
    }
    if (ch == '\\') {
        scanner.step = stateEndValueEsc;
        return .Continue;
    }
    if (ch < 0x20) {
        return scanner.setErr(Scanner.Error.InvalidCharacterInStringLiteral);
    }
    return .Continue;
}

fn stateEndValueEsc(scanner: *Scanner, ch: u8) Scanner.ScanState {
    return switch (ch) {
        'b', 'f', 'n', 'r', 't', '\\', '/', '"' => {
            scanner.step = stateInString;
            return .Continue;
        },
        'u' => {
            scanner.step = stateInString;
            return .Continue;
        },
        else => scanner.setErr(Scanner.Error.InvalidCharacterInStringEscapeCode),
    };
}

fn stateInStringEscU(scanner: *Scanner, ch: u8) Scanner.ScanState {
    if (isHex(ch)) {
        scanner.step = stateInStringEscU1;
        return .Continue;
    }
    return scanner.setErr(Scanner.Error.InvalidCharacterInHexCharEscape);
}

fn stateInStringEscU1(scanner: *Scanner, ch: u8) Scanner.ScanState {
    if (isHex(ch)) {
        scanner.step = stateInStringEscU12;
        return .Continue;
    }
    return scanner.setErr(Scanner.Error.InvalidCharacterInHexCharEscape);
}

fn stateInStringEscU12(scanner: *Scanner, ch: u8) Scanner.ScanState {
    if (isHex(ch)) {
        scanner.step = stateInStringEscU123;
        return .Continue;
    }
    return scanner.setErr(Scanner.Error.InvalidCharacterInHexCharEscape);
}

fn stateInStringEscU123(scanner: *Scanner, ch: u8) Scanner.ScanState {
    if (isHex(ch)) {
        scanner.step = stateInString;
        return .Continue;
    }
    return scanner.setErr(Scanner.Error.InvalidCharacterInHexCharEscape);
}

fn stateNeg(scanner: *Scanner, ch: u8) Scanner.ScanState {
    if (ch == '0') {
        scanner.step = state0;
        return .Continue;
    }
    if (('1' <= ch) and (ch <= '9')) {
        scanner.step = state1;
        return .Continue;
    }
    return scanner.setErr(Scanner.Error.InvalidCharacterInNumericLiteral);
}

fn stateRedo(scanner: *Scanner, _: u8) Scanner.ScanState {
    scanner.redo = false;
    scanner.step = scanner.redoState;
    return scanner.redoCode;
}

fn state0(scanner: *Scanner, ch: u8) Scanner.ScanState {
    if (ch == '.') {
        scanner.step = stateDot;
        return .Continue;
    }
    if ((ch == 'e') or (ch == 'E')) {
        scanner.step = stateE;
        return .Continue;
    }
    return stateEndValue(scanner, ch);
}

fn stateDot(scanner: *Scanner, ch: u8) Scanner.ScanState {
    if (('1' <= ch) and (ch <= '9')) {
        scanner.step = stateDot0;
        return .Continue;
    }
    return scanner.setErr(Scanner.Error.InvalidCharacterInHexCharEscape);
}

fn stateDot0(scanner: *Scanner, ch: u8) Scanner.ScanState {
    if (('1' <= ch) and (ch <= '9')) {
        return .Continue;
    }
    if ((ch == 'e') or (ch == 'E')) {
        scanner.step = stateE;
        return .Continue;
    }
    return stateESign(scanner, ch);
}

fn stateESign(scanner: *Scanner, ch: u8) Scanner.ScanState {
    if (('1' <= ch) and (ch <= '9')) {
        scanner.step = stateE0;
        return .Continue;
    }
    return scanner.setErr(Scanner.Error.InvalidCharacterInExponentOfNumericLiteral);
}

fn stateE0(scanner: *Scanner, ch: u8) Scanner.ScanState {
    if (('1' <= ch) and (ch <= '9')) {
        return .Continue;
    }
    return stateEndValue(scanner, ch);
}

fn stateE(scanner: *Scanner, ch: u8) Scanner.ScanState {
    if ((ch == '+') or (ch == '-')) {
        scanner.step = stateESign;
        return .Continue;
    }
    return .Continue;
}

fn state1(scanner: *Scanner, ch: u8) Scanner.ScanState {
    if (('1' <= ch) and (ch <= '9')) {
        scanner.step = state1;
        return .Continue;
    }
    return state0(scanner, ch);
}

fn stateF(scanner: *Scanner, ch: u8) Scanner.ScanState {
    if (ch == 'a') {
        scanner.step = stateFa;
        return .Continue;
    }
    return scanner.setErr(Scanner.Error.InvalidCharacterInLteralFalseExpectingA);
}

fn stateFa(scanner: *Scanner, ch: u8) Scanner.ScanState {
    if (ch == 'l') {
        scanner.step = stateFal;
        return .Continue;
    }
    return scanner.setErr(Scanner.Error.InvalidCharacterInLteralFalseExpectingL);
}

fn stateFal(scanner: *Scanner, ch: u8) Scanner.ScanState {
    if (ch == 's') {
        scanner.step = stateFals;
        return .Continue;
    }
    return scanner.setErr(Scanner.Error.InvalidCharacterInLteralFalseExpectingS);
}

fn stateFals(scanner: *Scanner, ch: u8) Scanner.ScanState {
    if (ch == 'e') {
        scanner.step = stateEndValue;
        return .Continue;
    }
    return scanner.setErr(Scanner.Error.InvalidCharacterInLteralFalseExpectingE);
}

fn stateN(scanner: *Scanner, ch: u8) Scanner.ScanState {
    if (ch == 'u') {
        scanner.step = stateNu;
        return .Continue;
    }
    return scanner.setErr(Scanner.Error.InvalidCharacterInLteralNullExpectingU);
}

fn stateNu(scanner: *Scanner, ch: u8) Scanner.ScanState {
    if (ch == 'l') {
        scanner.step = stateNul;
        return .Continue;
    }
    return scanner.setErr(Scanner.Error.InvalidCharacterInLteralNullExpectingL);
}

fn stateNul(scanner: *Scanner, ch: u8) Scanner.ScanState {
    if (ch == 'l') {
        scanner.step = stateEndValue;
        return .Continue;
    }
    return scanner.setErr(Scanner.Error.InvalidCharacterInLteralNullExpectingL);
}

fn stateT(scanner: *Scanner, ch: u8) Scanner.ScanState {
    if (ch == 'r') {
        scanner.step = stateTr;
        return .Continue;
    }
    return scanner.setErr(Scanner.Error.InvalidCharacterInLteralTrueExpectingR);
}

fn stateTr(scanner: *Scanner, ch: u8) Scanner.ScanState {
    if (ch == 'u') {
        scanner.step = stateTru;
        return .Continue;
    }
    return scanner.setErr(Scanner.Error.InvalidCharacterInLteralTrueExpectingU);
}

fn stateTru(scanner: *Scanner, ch: u8) Scanner.ScanState {
    if (ch == 'e') {
        scanner.step = stateEndValue;
        return .Continue;
    }
    return scanner.setErr(Scanner.Error.InvalidCharacterInLteralTrueExpectingE);
}

fn resetState(scanner: *Scanner, ch: u8) Scanner.ScanState {
    _ = scanner;
    _ = ch;
    return .Continue;
}

fn isHex(ch: u8) bool {
    return (('0' <= ch) and (ch <= '9')) or (('a' <= ch) and (ch <= 'f')) or (('A' <= ch) and (ch <= 'F'));
}

fn isSpace(ch: u8) bool {
    return (ch == ' ') or (ch == '\t') or (ch == '\r') or (ch == '\n');
}

fn quoteChar(ch: u8) [3:0]u8 {
    if (ch == '\'') {
        return .{ '\'', 0, 0 };
    }
    if (ch == '"') {
        return .{ '"', 0, 0 };
    }
    return .{ '\'', ch, '\'' };
}

fn quoteCharLen(ch: u8) usize {
    if ((ch == '\'') or (ch == '"')) {
        return 1;
    }
    return 3;
}

test "scan_valid" {
    try std.testing.expect(valid(std.testing.allocator, "foo") == false);
    try std.testing.expect(valid(std.testing.allocator, "}{") == false);
    //try std.testing.expect(valid(std.testing.allocator, "{]") == false);
    //try std.testing.expect(valid(std.testing.allocator, "{}") == true);
    //try std.testing.expect(valid(std.testing.allocator, "foo") == false);
}

test "scanner_is_resetable" {
    var scanner = Scanner.new(std.testing.allocator);
    scanner.reset();
    try std.testing.expectEqual(scanner.err, Scanner.Error.None);
    try std.testing.expect(!scanner.redo);
    try std.testing.expect(!scanner.endTop);
}

test "scanner_eof" {
    var scanner = Scanner.new(std.testing.allocator);
    scanner.reset();
    try std.testing.expectEqual(scanner.eof(), Scanner.ScanState.Error);
}

test "scanner_pushParseState" {
    var scanner = Scanner.new(std.testing.allocator);
    defer scanner.deinit();
    scanner.reset();
    try scanner.pushParseState(Scanner.ParseState.ObjectKey);
    try std.testing.expectEqual(scanner.parseState.items[0], Scanner.ParseState.ObjectKey);
}

test "scanner_popParseState" {
    var scanner = Scanner.new(std.testing.allocator);
    defer scanner.deinit();
    scanner.reset();
    try scanner.pushParseState(Scanner.ParseState.ObjectKey);
    try std.testing.expectEqual(scanner.parseState.items.len, 1);
    scanner.popParseState();
    try std.testing.expectEqual(scanner.parseState.items.len, 0);
    try std.testing.expect(scanner.endTop);
}

test "scanner_stateBeginValue" {
    var s = Scanner.new(std.testing.allocator);
    defer s.deinit();
    try std.testing.expectEqual(stateBeginValue(&s, ' '), .SkipSpace);
    try std.testing.expectEqual(stateBeginValue(&s, '{'), .BeginObject);
    try std.testing.expectEqual(stateBeginValue(&s, '['), .BeginArray);
    try std.testing.expectEqual(stateBeginValue(&s, '"'), .BeginLiteral);
    try std.testing.expectEqual(stateBeginValue(&s, '-'), .BeginLiteral);
    try std.testing.expectEqual(stateBeginValue(&s, '0'), .BeginLiteral);
    try std.testing.expectEqual(stateBeginValue(&s, 't'), .BeginLiteral);
    try std.testing.expectEqual(stateBeginValue(&s, 'f'), .BeginLiteral);
    try std.testing.expectEqual(stateBeginValue(&s, 'n'), .BeginLiteral);
    try std.testing.expectEqual(stateBeginValue(&s, '1'), .BeginLiteral);
    try std.testing.expectEqual(stateBeginValue(&s, '2'), .BeginLiteral);
    try std.testing.expectEqual(stateBeginValue(&s, '3'), .BeginLiteral);
    try std.testing.expectEqual(stateBeginValue(&s, '4'), .BeginLiteral);
    try std.testing.expectEqual(stateBeginValue(&s, '5'), .BeginLiteral);
    try std.testing.expectEqual(stateBeginValue(&s, '6'), .BeginLiteral);
    try std.testing.expectEqual(stateBeginValue(&s, '7'), .BeginLiteral);
    try std.testing.expectEqual(stateBeginValue(&s, '8'), .BeginLiteral);
    try std.testing.expectEqual(stateBeginValue(&s, '9'), .BeginLiteral);
    try std.testing.expectEqual(stateBeginValue(&s, 'x'), .Error);
}

test "scanner_stateBeginStringOrEmpty" {
    var s = Scanner.new(std.testing.allocator);
    defer s.deinit();
    try s.pushParseState(Scanner.ParseState.ObjectValue);
    try std.testing.expectEqual(stateBeginStringOrEmpty(&s, ' '), .SkipSpace);
    try std.testing.expectEqual(stateBeginStringOrEmpty(&s, '}'), .EndObject);
    try std.testing.expectEqual(stateBeginStringOrEmpty(&s, 'x'), .Error);
}

test "scanner_stateBeginString" {
    var s = Scanner.new(std.testing.allocator);
    defer s.deinit();
    try s.pushParseState(Scanner.ParseState.ObjectValue);
    try std.testing.expectEqual(stateBeginString(&s, ' '), .SkipSpace);
    try std.testing.expectEqual(stateBeginString(&s, '"'), .BeginLiteral);
    try std.testing.expectEqual(stateBeginString(&s, 'x'), .Error);
}

test "scanner_stateEndTop" {
    var s = Scanner.new(std.testing.allocator);
    defer s.deinit();
    try s.pushParseState(Scanner.ParseState.ObjectValue);
    try std.testing.expectEqual(stateEndTop(&s, ' '), .Error);
    try std.testing.expectEqual(stateEndTop(&s, '\t'), .End);
    try std.testing.expectEqual(stateEndTop(&s, '\r'), .End);
    try std.testing.expectEqual(stateEndTop(&s, '\n'), .End);
    try std.testing.expectEqual(stateEndTop(&s, 'x'), .End);
}

test "scanner_stateEndValue" {
    var s = Scanner.new(std.testing.allocator);
    defer s.deinit();
    try std.testing.expectEqual(stateEndValue(&s, 'x'), .End);
    try s.pushParseState(Scanner.ParseState.ObjectKey);
    try std.testing.expectEqual(stateEndValue(&s, ' '), .SkipSpace);
    try std.testing.expectEqual(stateEndValue(&s, ':'), .ObjectKey);
    try std.testing.expectEqual(stateEndValue(&s, 'x'), .Error);
    s.popParseState();
    try s.pushParseState(Scanner.ParseState.ObjectValue);
    try std.testing.expectEqual(stateEndValue(&s, ':'), .ObjectValue);
    s.popParseState();
    try s.pushParseState(Scanner.ParseState.ObjectValue);
    try std.testing.expectEqual(stateEndValue(&s, '}'), .EndObject);
    try s.pushParseState(Scanner.ParseState.ObjectValue);
    try std.testing.expectEqual(stateEndValue(&s, 'x'), .Error);
    s.popParseState();
    try s.pushParseState(Scanner.ParseState.ArrayValue);
    try std.testing.expectEqual(stateEndValue(&s, ','), .ArrayValue);
    try std.testing.expectEqual(stateEndValue(&s, '}'), .EndArray);
    try s.pushParseState(Scanner.ParseState.ArrayValue);
    try std.testing.expectEqual(stateEndValue(&s, 'x'), .Error);
}

test "scanner_stateError" {
    var s = Scanner.new(std.testing.allocator);
    defer s.deinit();
    try std.testing.expectEqual(stateError(&s, 'x'), .Error);
}

test "scanner_stateInString" {
    var s = Scanner.new(std.testing.allocator);
    defer s.deinit();
    try std.testing.expectEqual(stateInString(&s, '"'), .Continue);
    try std.testing.expectEqual(stateInString(&s, '\\'), .Continue);
    try std.testing.expectEqual(stateInString(&s, 0x1f), .Error);
    try std.testing.expectEqual(stateInString(&s, 0x20), .Continue);
}

test "scanner_stateEndValueEsc" {
    var s = Scanner.new(std.testing.allocator);
    defer s.deinit();
    try std.testing.expectEqual(stateEndValueEsc(&s, 'x'), .Error);
    try std.testing.expectEqual(stateEndValueEsc(&s, 'b'), .Continue);
    try std.testing.expectEqual(stateEndValueEsc(&s, 'f'), .Continue);
    try std.testing.expectEqual(stateEndValueEsc(&s, 'n'), .Continue);
    try std.testing.expectEqual(stateEndValueEsc(&s, 'r'), .Continue);
    try std.testing.expectEqual(stateEndValueEsc(&s, 't'), .Continue);
    try std.testing.expectEqual(stateEndValueEsc(&s, '\\'), .Continue);
    try std.testing.expectEqual(stateEndValueEsc(&s, '/'), .Continue);
    try std.testing.expectEqual(stateEndValueEsc(&s, '"'), .Continue);
    try std.testing.expectEqual(stateEndValueEsc(&s, 'u'), .Continue);
}

test "scanner_stateInStringEscUN" {
    var s = Scanner.new(std.testing.allocator);
    defer s.deinit();
    const ch1 = 'f'; // hex
    try std.testing.expectEqual(stateInStringEscU(&s, ch1), .Continue);
    try std.testing.expectEqual(stateInStringEscU1(&s, ch1), .Continue);
    try std.testing.expectEqual(stateInStringEscU12(&s, ch1), .Continue);
    try std.testing.expectEqual(stateInStringEscU123(&s, ch1), .Continue);
    const ch2 = 'g'; // not hex
    try std.testing.expectEqual(stateInStringEscU(&s, ch2), .Error);
    try std.testing.expectEqual(stateInStringEscU1(&s, ch2), .Error);
    try std.testing.expectEqual(stateInStringEscU12(&s, ch2), .Error);
    try std.testing.expectEqual(stateInStringEscU123(&s, ch2), .Error);
}

test "scanner_stateNeg" {
    var s = Scanner.new(std.testing.allocator);
    defer s.deinit();
    try std.testing.expectEqual(stateNeg(&s, '0'), .Continue);
    try std.testing.expectEqual(stateNeg(&s, '9'), .Continue);
    try std.testing.expectEqual(stateNeg(&s, 'a'), .Error);
}

test "quoteChar" {
    try std.testing.expectEqualStrings(quoteChar('a')[0..quoteCharLen('a')], "'a'");
    var str = quoteChar('"');
    try std.testing.expectEqualStrings(str[0..quoteCharLen('"')], "\"");
    str = quoteChar('\'');
    try std.testing.expectEqualStrings(str[0..quoteCharLen('\'')], "'");
}
